"use strict"
const fs = require('fs');
let read  = file => {
	return new Promise((resolve,reject)=>{
		fs.readFile(file, "utf8", (err, data)=>{
			if(err)
				reject(err);
			else
				resolve(data);
		});
	})
};
let write  = (file, data) => {
	return new Promise((resolve,reject)=>{
		fs.writeFile(file,data,"utf8", (err)=>{
			if(err)
				reject(err);
			else
				resolve(`${file}`);
		});
	})
};
module.exports = {
read,
write
};
"use strict";
const fs = require("fs");

let pathInfo = (path, callback) => {
  let info = {};
  info.path = path;

  fs.stat(path, (err, type) => {
    if (err) throw err;
    if (type.isFile()) {
      info.type = "file";
      fs.readFile(path, "utf8", (err, content) => {
        info.content = content;
        callback(err, info);
      });
    }
    if (type.isDirectory()) {
      info.type = "directory";
      fs.readdir(path, (err, childs) => {
        info.childs = childs;
        callback(err, info);
      });
    }
  });
};
module.exports = pathInfo;
"use strict";
const fs = require("fs");

/* Функция, чтения и возврата данных файла*/
let read_f = (path, file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path + file, "utf8", (err, content) => {
      if (err)
        reject(err);
      else
        resolve(content);
    });
  })
};

/* Функция, чтения директории и получения массива названий файлов*/
let readAll = (path) => {
  return new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      if (err)
        reject(err);
      else
        resolve(files);
    });
  })
  .then(files => Promise.all(
    files.map(
      file => {
        return read_f(path, file) //получение данных файлов
          .then(content => {
            return {content, file}
          })
      }
    )
  ))
  .then(files => {
    return files.map(file => {
      return {name: file.file, content: file.content} //создание массива соответствия данных файла и имени
    })
  })
};
module.exports = readAll;